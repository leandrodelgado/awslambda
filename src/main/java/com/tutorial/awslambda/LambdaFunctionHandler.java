package com.tutorial.awslambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class LambdaFunctionHandler implements RequestHandler<String, String> {
	@Override
	public String handleRequest(String nombre, Context context) {

		return "Hola " + nombre + "!!";
	}
}
